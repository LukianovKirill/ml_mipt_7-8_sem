# -*- coding: utf-8 -*-
"""Lab1_part1_differentiation.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/github/girafe-ai/ml-mipt/blob/basic_f20/homeworks_basic/Lab1_ML_pipeline_and_SVM/Lab1_part1_differentiation.ipynb

*Credits: materials from this notebook belong to YSDA [Practical DL](https://github.com/yandexdataschool/Practical_DL) course. Special thanks for making them available online.*

# Lab assignment №1, part 1

This lab assignment consists of several parts. You are supposed to make some transformations, train some models, estimate the quality of the models and explain your results.

Several comments:
* Don't hesitate to ask questions, it's a good practice.
* No private/public sharing, please. The copied assignments will be graded with 0 points.
* Blocks of this lab will be graded separately.

## Part 1. Matrix differentiation

Since it easy to google every task please please please try to undestand what's going on. The "just answer" thing will be not counted, make sure to present derivation of your solution. It is absolutely OK if you found an answer on web then just exercise in $\LaTeX$ copying it into here.
"""

# If on colab, uncomment the following lines

# ! wget https://raw.githubusercontent.com/girafe-ai/ml-mipt/basic_f20/homeworks_basic/Lab1_ML_pipeline_and_SVM/grad.png

"""Useful links: 
[1](http://www.machinelearning.ru/wiki/images/2/2a/Matrix-Gauss.pdf)
[2](http://www.atmos.washington.edu/~dennis/MatrixCalculus.pdf)
[3](http://cal.cs.illinois.edu/~johannes/research/matrix%20calculus.pdf)
[4](http://research.microsoft.com/en-us/um/people/cmbishop/prml/index.htm)

## ex. 1

$$  
y = x^Tx,  \quad x \in \mathbb{R}^N 
$$

$$
\frac{dx^Tx}{dx_1} = \frac{d}{dx_1} (\sum x^2_i) = \frac{d}{dx_1} x^2_1 = 2x_1
$$ 
and similarly for each of the other components of x
$$
\frac{dy}{dx} = 2x
$$
"""



"""## ex. 2

$$ y = tr(AB) \quad A,B \in \mathbb{R}^{N \times N} $$

$$
\frac{tr(AB)}{da_{12}} = \frac{d}{da_{12}} (\sum_{0<i,j<n} a_{ij}b_{ji}) = \frac{d}{da_{12}} a_{12}b_{21} = b_{21}
$$ 
and similarly for each of the other components of $a_{ij}$
$$
\frac{dy}{dA} = B^T
$$
"""



"""## ex. 3

$$  
y = x^TAc , \quad A\in \mathbb{R}^{N \times N}, x\in \mathbb{R}^{N}, c\in \mathbb{R}^{N} 
$$

Define $w^T = x^TA,y = w^Tc, \frac{dy}{dc} = w^T$

$$
y = y^T = c^TA^Tx, \frac{d(c^TA^Tx)}{dx} = c^TA^T
$$

$$
\frac{dy}{dx} = c^TA^T
$$

$$
\frac{x^TAc}{da_{12}} = \frac{d}{da_{12}} (\sum_{0<i,j<n} x_{1i}a_{ij}c_{j1}) = \frac{d}{da_{12}} x_{11}a_{12}c_{21} = x_{11}c_{21}
$$

$$
\frac{dy}{dA} = xc^T
$$

Hint for the latter (one of the ways): use *ex. 2* result and the fact 
$$
tr(ABC) = tr (CAB)
$$
"""



"""## ex. 4

Classic matrix factorization example. Given matrix $X$ you need to find $A$, $S$ to approximate $X$. This can be done by simple gradient descent iteratively alternating $A$ and $S$ updates.
$$
J = || X - AS ||_F^2  , \quad A\in \mathbb{R}^{N \times R} , \quad S\in \mathbb{R}^{R \times M}
$$
$$
\frac{dJ}{dS} = ? 
$$

You may use one of the following approaches:

#### First approach
Using ex.2 and the fact:
$$
|| X ||_F^2 = tr(XX^T) 
$$ 
it is easy to derive gradients (you can find it in one of the refs).

#### Second approach
You can use *slightly different techniques* if they suits you. Take a look at this derivation:
<img src="grad.png">
(excerpt from [Handbook of blind source separation, Jutten, page 517](https://books.google.ru/books?id=PTbj03bYH6kC&printsec=frontcover&dq=Handbook+of+Blind+Source+Separation&hl=en&sa=X&ved=0ahUKEwi-q_apiJDLAhULvXIKHVXJDWcQ6AEIHDAA#v=onepage&q=Handbook%20of%20Blind%20Source%20Separation&f=false), open for better picture).

#### Third approach
And finally we can use chain rule! 
let $ F = AS $ 

**Find**
$$
J = \frac{1}{2}tr((X-AS)^T(X-AS))=\frac{1}{2}tr(X^TX-(AS)^T-X^TAS+(AS)^TAS)
$$

Аналогично действиям из 2 упражнения $\frac{X^TX}{dAS} = 0, \frac{-(AS)^TX}{dAS} = -X, \frac{-X^TAS}{dAS} = -X, \frac{(AS)^TAS}{dAS} = 2AS$

$$
\frac{dJ}{dF} =  -X+AS
$$ 
and 
$$
\frac{dF}{dS} =  A^T
$$ 
(the shape should be $ NM \times RM )$.

Now it is easy do get desired gradients:
$$
\frac{dJ}{dS} =  A^T(-X+AS) = -A^TX+A^TAS
$$
"""

