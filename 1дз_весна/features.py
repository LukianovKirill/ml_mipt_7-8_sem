from collections import OrderedDict
from sklearn.base import TransformerMixin
from typing import List, Union
import numpy as np
import math


class BoW(TransformerMixin):
    """
    Bag of words tranformer class
    
    check out:
    https://scikit-learn.org/stable/modules/generated/sklearn.base.TransformerMixin.html
    to know about TransformerMixin class
    """

    def __init__(self, k: int):
        """
        :param k: number of most frequent tokens to use
        """
        self.k = k
        # list of k most frequent tokens
        self.bow = None
        

    def fit(self, X: np.ndarray, y=None):
        """
        :param X: array of texts to be trained on
        """
        # task: find up to self.k most frequent tokens in texts_train,
        # sort them by number of occurences (highest first)
        # store most frequent tokens in self.bow
        all_words = []
        for text in X:
            splitted = text.split(" ")
            for word in splitted:
                all_words.append(word)
                
        def wordListToFreqDict(wordlist):
            wordfreq = [wordlist.count(p) for p in wordlist]
            return dict(list(zip(wordlist,wordfreq))) 
        wd = wordListToFreqDict(all_words)
        
        def sortFreqDict(freqdict):
            aux = [(freqdict[key], key) for key in freqdict]
            aux.sort()
            aux.reverse()
            return aux
        sd = sortFreqDict(wd)
        
        l = []
        for i in range(self.k):
            l.append(sd[i][1])
            
        self.bow = l
        #raise NotImplementedError

        # fit method must always return self
        return self

    def _text_to_bow(self, text: str) -> np.ndarray:
        """
        convert text string to an array of token counts. Use self.bow.
        :param text: text to be transformed
        :return bow_feature: feature vector, made by bag of words
        """
        result = np.zeros(self.k)
        words = text.split(" ")
        for w in words:
            if(w in self.bow):
                result[self.bow.index(w)] += 1
        
        return np.array(result, "float32")

    def transform(self, X: np.ndarray, y=None) -> np.ndarray:
        """
        :param X: array of texts to transform
        :return: array of transformed texts
        """
        assert self.bow is not None
        return np.stack([self._text_to_bow(text) for text in X])

    def get_vocabulary(self) -> Union[List[str], None]:
        return self.bow


class TfIdf(TransformerMixin):
    """
    Tf-Idf tranformer class
    if you have troubles implementing Tf-Idf, check out:
    https://streamsql.io/blog/tf-idf-from-scratch
    """

    def __init__(self, k: int = None, normalize: bool = False):
        """
        :param k: number of most frequent tokens to use
        if set k equals None, than all words in train must be considered
        :param normalize: if True, you must normalize each data sample
        after computing tf-idf features
        """
        self.k = k
        self.normalize = normalize
        self.Countdict = None

        # self.idf[term] = log(total # of documents / # of documents with term in it)
        self.idf = OrderedDict()

    def fit(self, X: np.ndarray, y=None):
        """
        :param X: array of texts to be trained on
        """
        
        all_words = []
        for text in X:
            splitted = text.split(" ")
            all_words.append(splitted)
        
        def computeReviewTFDict(review):
            reviewTFDict = {}
            for word in review:
                if word in reviewTFDict:
                    reviewTFDict[word] += 1
                else:
                    reviewTFDict[word] = 1
            # Computes tf for each word
            for word in reviewTFDict:
                reviewTFDict[word] = reviewTFDict[word] / len(review)
            return reviewTFDict
        
        tfDict = []
        for text in all_words:
            rewD = computeReviewTFDict(text)
            tfDict.append(rewD)
            
        def computeCountDict():
            countDict = {}
            for review in tfDict:
                for word in review:
                    if word in countDict:
                        countDict[word] += 1
                    else:
                        countDict[word] = 1
            return countDict
        
        #gathering sorted by frequency all words dict
        countDict = computeCountDict()
        
        self.Countdict = sorted(countDict.keys())
        if(self.k != None):
            self.Countdict = self.Countdict[:self.k]
        
            
        
        def computeIDFDict():
            idfDict = {}
            for word in countDict:
                idfDict[word] = math.log(len(X) / countDict[word])
            return idfDict
  
        idfDict = computeIDFDict()
        self.idf = idfDict
        

        # fit method must always return self
        return self

    def _text_to_tf_idf(self, text: str) -> np.ndarray:
        """
        convert text string to an array tf-idfs.
        *Note* don't forget to normalize, when self.normalize == True
        :param text: text to be transformed
        :return tf_idf: tf-idf features
        """
        def computeReviewTFDict(review):
            reviewTFDict = {}
            for word in review:
                if word in reviewTFDict:
                    reviewTFDict[word] += 1
                else:
                    reviewTFDict[word] = 1
            # Computes tf for each word
            for word in reviewTFDict:
                reviewTFDict[word] = reviewTFDict[word] / len(review)
            return reviewTFDict
        
        splitted = text.split(" ")
        rewD = computeReviewTFDict(splitted)
        
        def computeReviewTFIDFDict(reviewTFDict):
            reviewTFIDFDict = {}
            
            for word in reviewTFDict:
                if(word in self.idf):
                    reviewTFIDFDict[word] = reviewTFDict[word] * self.idf[word]
                else:
                    reviewTFIDFDict[word] = reviewTFDict[word] * math.log(500)
            return reviewTFIDFDict
        
        tfidfrew = computeReviewTFIDFDict(rewD)
        
        
        # Create a list of unique words
        wordDict = self.Countdict

        def computeTFIDFVector(review):
            tfidfVector = [0.0] * len(wordDict)

            # For each unique word, if it is in the review, store its TF-IDF value.
            for i, word in enumerate(wordDict):
                if word in review:
                    tfidfVector[i] = review[word]
            return tfidfVector

        tfidfVector = computeTFIDFVector(tfidfrew)

        result = tfidfVector
        norm = np.linalg.norm(result)
        if(self.normalize == True and norm != 0):
            result /= norm
      
        return np.array(result, "float32")

    def transform(self, X: np.ndarray, y=None) -> np.ndarray:
        """
        :param X: array of texts to transform
        :return: array of transformed texts
        """
        assert self.idf is not None
        return np.stack([self._text_to_tf_idf(text) for text in X])
