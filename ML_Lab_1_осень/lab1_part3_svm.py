# -*- coding: utf-8 -*-
"""Lab1_part3_SVM.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/github/girafe-ai/ml-mipt/blob/basic_f20/homeworks_basic/Lab1_ML_pipeline_and_SVM/Lab1_part3_SVM.ipynb

# Lab assignment №1, part 3

This lab assignment consists of several parts. You are supposed to make some transformations, train some models, estimate the quality of the models and explain your results.

Several comments:
* Don't hesitate to ask questions, it's a good practice.
* No private/public sharing, please. The copied assignments will be graded with 0 points.
* Blocks of this lab will be graded separately.

__*This is the third part of the assignment. First and second parts are waiting for you in the same directory.*__

##  Part 3. SVM and kernels

Kernels concept get adopted in variety of ML algorithms (e.g. Kernel PCA, Gaussian Processes, kNN, ...).

So in this task you are to examine kernels for SVM algorithm applied to rather simple artificial datasets.

To make it clear: we will work with the classification problem through the whole notebook.
"""

from sklearn.datasets import make_moons
import matplotlib.pyplot as plt
import numpy as np

"""Let's generate our dataset and take a look on it."""

moons_points, moons_labels = make_moons(n_samples=500, noise=0.2, random_state=42)
plt.scatter(moons_points[:, 0], moons_points[:, 1], c=moons_labels)

"""## 1.1 Pure models.
First let's try to solve this case with good old Logistic Regression and simple (linear kernel) SVM classifier.

Train LR and SVM classifiers (choose params by hand, no CV or intensive grid search neeeded) and plot their decision regions. Calculate one preffered classification metric.

Describe results in one-two sentences.

_Tip:_ to plot classifiers decisions you colud use either sklearn examples ([this](https://scikit-learn.org/stable/auto_examples/neural_networks/plot_mlp_alpha.html#sphx-glr-auto-examples-neural-networks-plot-mlp-alpha-py) or any other) and mess with matplotlib yourself or great [mlxtend](https://github.com/rasbt/mlxtend) package (see their examples for details)

_Pro Tip:_ wirte function `plot_decisions` taking a dataset and an estimator and plotting the results cause you want to use it several times below
"""

from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.metrics import f1_score

from mlxtend.plotting import plot_decision_regions

lr = LogisticRegression(C = 3) 
svm = SVC(kernel='linear', C = 3)

lr.fit(moons_points, moons_labels)
svm.fit(moons_points, moons_labels)

f1_lr = f1_score(lr.predict(moons_points), moons_labels)
f1_svm = f1_score(svm.predict(moons_points), moons_labels)
print(f1_lr, f1_svm)

plot_decision_regions(moons_points, moons_labels, clf=svm)
plt.show()

plot_decision_regions(moons_points, moons_labels, clf=lr)
plt.show()

"""Результаты достаточно неплохие учитывая, что задача очевидно требует не линейной разделяющей поверхности. Значительно улучшить результаты при линейной разделяющей поверхности не получится.

## 1.2 Kernel tirck

![keep-calm](imgs/keep-calm.jpg)

Now use different kernels (`poly`, `rbf`, `sigmoid`) on SVC to get better results. Play `degree` parameter and others.

For each kernel estimate optimal params, plot decision regions, calculate metric you've chosen eariler.

Write couple of sentences on:

* What have happenned with classification quality?
* How did decision border changed for each kernel?
* What `degree` have you chosen and why?
"""

svm_2 = SVC(kernel='poly', C = 3, gamma = 0.5, degree = 3) 
svm_3 = SVC(kernel='rbf', C = 1, gamma = 2)
svm_4 = SVC(kernel='sigmoid', C = 1, gamma = 0.05)

svm_2.fit(moons_points, moons_labels)
svm_3.fit(moons_points, moons_labels)
svm_4.fit(moons_points, moons_labels)

f1_svm_2 = f1_score(svm_2.predict(moons_points), moons_labels)
f1_svm_3 = f1_score(svm_3.predict(moons_points), moons_labels)
f1_svm_4 = f1_score(svm_4.predict(moons_points), moons_labels)

print(f1_svm_2, f1_svm_3, f1_svm_4)

plot_decision_regions(moons_points, moons_labels, clf=svm_2)
plt.show()

plot_decision_regions(moons_points, moons_labels, clf=svm_3)
plt.show()

plot_decision_regions(moons_points, moons_labels, clf=svm_4)
plt.show()

"""Как видно из графиков лучше всего для данной задачи подходит rbf ядро. Для rbf ядра я взял gamma = 2, так как это показывала достаточно хорошие результаты, конечно при более высокой степень f1 мера повышается, однако, это фактически приводит к переобучению, в данном случае мы знаем как распределенны данные и повышение стпени было бы уместно, но на практике так делать неправильно, поэтому я оставил менее качественный вариант, но который не так сильно подстраивается под данные. Другие ядра не слишком хорошо подходят для задачи, поэтому степени я брал при которых достигается наилучшие результаты. Для полиноминального и rbf ядер результаты улучшились, особенно для rbf, что вполне ожидаемо. Границы решения стали нелинейными, что так же абсолюно ожидаемо.

## 1.3 Simpler solution (of a kind)
What is we could use Logisitc Regression to successfully solve this task?

Feature generation is a thing to help here. Different techniques of feature generation are used in real life, couple of them will be covered in additional lectures.

In particular case simple `PolynomialFeatures` ([link](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.PolynomialFeatures.html)) are able to save the day.

Generate the set of new features, train LR on it, plot decision regions, calculate metric.

* Comare SVM's results with this solution (quality, borders type)
* What degree of PolynomialFeatures have you used? Compare with same SVM kernel parameter.
"""

from sklearn.preprocessing import PolynomialFeatures

poly = PolynomialFeatures(3)
poly.fit(moons_points, moons_labels)
poly_x =poly.fit_transform(moons_points)

lr_poly = LogisticRegression(C = 3)
lr_poly.fit(poly_x, moons_labels) 
f1_poly = f1_score(lr_poly.predict(poly_x), moons_labels)
print(f1_poly)

"""Введение полиномиальных features помогло и значительно улучшило результаты класификатора. Изменение качество в зависимости от выбора степени аналогично степени rbf ядра, сначала идет значительное улучшение качества при вводе 3 степени features, а после увеличение хоть и приводит к улучшению, но во-первых не дает значительного улучшения качества, а во-вторых приводило бы к переобучению на практике. Поэтому выбор степени равный 3 был сделан из аналогичных соображений, что и в случае с rbf ядром.

## 1.4 Harder problem

Let's make this task a bit more challenging via upgrading dataset:
"""

from sklearn.datasets import make_circles

circles_points, circles_labels = make_circles(n_samples=500, noise=0.06, random_state=42)

plt.figure(figsize=(5, 5))
plt.scatter(circles_points[:, 0], circles_points[:, 1], c=circles_labels)

"""And even more:"""

points = np.vstack((circles_points*2.5 + 0.5, moons_points))
labels = np.hstack((circles_labels, moons_labels + 2)) # + 2 to distinct moons classes

plt.figure(figsize=(5, 5))
plt.scatter(points[:, 0], points[:, 1], c=labels)

"""Now do your best using all the approaches above!

Tune LR with generated features, SVM with appropriate kernel of your choice. You may add some of your loved models to demonstrate their (and your) strength. Again plot decision regions, calculate metric.

Justify the results in a few phrases.
"""

svm_mult = SVC(kernel='rbf', C = 1, gamma = 4)
svm_mult.fit(points, labels)

f1_mult = f1_score(svm_mult.predict(points), labels, average='macro')
print(f1_mult)

plot_decision_regions(points, labels, clf = svm_mult)
plt.show()