# -*- coding: utf-8 -*-
"""Lab1_part2_ml_pipeline.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/github/girafe-ai/ml-mipt/blob/basic_f20/homeworks_basic/Lab1_ML_pipeline_and_SVM/Lab1_part2_ml_pipeline.ipynb

# Lab assignment №1, part 2

This lab assignment consists of several parts. You are supposed to make some transformations, train some models, estimate the quality of the models and explain your results.

Several comments:
* Don't hesitate to ask questions, it's a good practice.
* No private/public sharing, please. The copied assignments will be graded with 0 points.
* Blocks of this lab will be graded separately.

__*This is the second part of the assignment. First and third parts are waiting for you in the same directory.*__

## Part 2. Data preprocessing, model training and evaluation.

### 1. Reading the data
Today we work with the [dataset](https://archive.ics.uci.edu/ml/datasets/Statlog+%28Vehicle+Silhouettes%29), describing different cars for multiclass ($k=4$) classification problem. The data is available below.
"""

# If on colab, uncomment the following lines

!wget https://raw.githubusercontent.com/girafe-ai/ml-mipt/basic_f20/homeworks_basic/Lab1_ML_pipeline_and_SVM/car_data.csv

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split

dataset = pd.read_csv('car_data.csv', delimiter=',', header=None).values
data = dataset[:, :-1].astype(int)
target = dataset[:, -1]

print(data.shape, target.shape)

X_train, X_test, y_train, y_test = train_test_split(data, target, test_size=0.35)
print(X_train.shape, y_train.shape, X_test.shape, y_test.shape)

"""To get some insights about the dataset, `pandas` might be used. The `train` part is transformed to `pd.DataFrame` below."""

X_train_pd = pd.DataFrame(X_train)

# First 15 rows of our dataset.
X_train_pd.head(15)

"""Methods `describe` and `info` deliver some useful information."""

X_train_pd.describe()

X_train_pd.info()

"""### 2. Machine Learning pipeline
Here you are supposed to perform the desired transformations. Please, explain your results briefly after each task.

#### 2.0. Data preprocessing
* Make some transformations of the dataset (if necessary). Briefly explain the transformations
"""

X_train_pd_norm = X_train_pd
X_train_pd_norm = ((X_train_pd - X_train_pd.min()) / (X_train_pd[:].max() - X_train_pd[:].min()) - 0.5)

X_train_pd.describe()

X_train_pd_norm.describe()

"""Я сделал нормализацию данных и их центрирование относительно 0. Что при введение регуляризации в моделях позволит оценивать все признаки одинаково качественно.

#### 2.1. Basic logistic regression
* Find optimal hyperparameters for logistic regression with cross-validation on the `train` data (small grid/random search is enough, no need to find the *best* parameters).

* Estimate the model quality with `f1` and `accuracy` scores.
* Plot a ROC-curve for the trained model. For the multiclass case you might use `scikitplot` library (e.g. `scikitplot.metrics.plot_roc(test_labels, predicted_proba)`).

*Note: please, use the following hyperparameters for logistic regression: `multi_class='multinomial'`, `solver='saga'` `tol=1e-3` and ` max_iter=500`.*
"""

from sklearn import linear_model as lin_mod
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score

clf_lr = lin_mod.LogisticRegression(multi_class='multinomial', solver='saga', tol=1e-3, max_iter=500)
clf_lr.fit(X_train_pd_norm, y_train)

f1_lr = f1_score(clf_lr.predict(X_test), y_test, average='micro')
acc = accuracy_score(clf_lr.predict(X_test), y_test)
print(f1_lr, acc)

# You might use this command to install scikit-plot. 
# Warning, if you a running locally, don't call pip from within jupyter, call it from terminal in the corresponding 
# virtual environment instead

# ! pip install scikit-plot

"""#### 2.2. PCA: explained variance plot
* Apply the PCA to the train part of the data. Build the explaided variance plot.
"""

from sklearn.decomposition import PCA

pca = PCA(n_components = 8)

"""#### 2.3. PCA trasformation
* Select the appropriate number of components. Briefly explain your choice. Should you normalize the data?

*Use `fit` and `transform` methods to transform the `train` and `test` parts.*
"""

pca.fit(X_train_pd)
X_train_pd_PCA = pca.fit_transform(X_train_pd)
pca.fit(X_test)
X_test_PCA = pca.fit_transform(X_test)

pca.fit(data)
data_PCA = pca.fit_transform(data)
X_train_PCA, X_test_PCA, y_train, y_test = train_test_split(data_PCA, target, test_size=0.35)

"""Я выбрал количесво n_components в соответствии с количество features у которых десперсия превышала 10. 10, так как таких features было 8, что больше 1/4, но меньше 1/2 от общего количества.

**Note: From this point `sklearn` [Pipeline](https://scikit-learn.org/stable/modules/compose.html) might be useful to perform transformations on the data. Refer to the [docs](https://scikit-learn.org/stable/modules/generated/sklearn.pipeline.Pipeline.html) for more information.**

#### 2.4. Logistic regression on PCA-preprocessed data.
* Find optimal hyperparameters for logistic regression with cross-validation on the transformed by PCA `train` data.

* Estimate the model quality with `f1` and `accuracy` scores.
* Plot a ROC-curve for the trained model. For the multiclass case you might use `scikitplot` library (e.g. `scikitplot.metrics.plot_roc(test_labels, predicted_proba)`).

*Note: please, use the following hyperparameters for logistic regression: `multi_class='multinomial'`, `solver='saga'` and `tol=1e-3`*
"""

clf_lr2 = lin_mod.LogisticRegression(multi_class='multinomial', solver='saga', tol=1e-3, max_iter=500)
clf_lr2.fit(X_train_PCA, y_train)

f1_lr2 = f1_score(clf_lr2.predict(X_test_PCA), y_test, average='macro')
acc_lr2 = accuracy_score(clf_lr2.predict(X_test_PCA), y_test)
print(f1_lr2, acc_lr2)

"""#### 2.5. Decision tree
* Now train a desicion tree on the same data. Find optimal tree depth (`max_depth`) using cross-validation.

* Measure the model quality using the same metrics you used above.
"""

from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import cross_validate

score_f1 = []
score_acc = []
for i in range(1, 50):
  DTC = DecisionTreeClassifier(max_depth = i)
  res = cross_validate(DTC, data_PCA, target, cv = 5, scoring = ['f1_macro', 'accuracy'])
  score_f1.append(res['test_f1_macro'].mean())
  score_acc.append(res['test_accuracy'].mean())

import matplotlib.pyplot as plt

x = [j for j in range(1, 50)]
plt.plot(x, score_f1)

x = [j for j in range(1, 50)]
plt.plot(x, score_acc)

"""Как видно из графиков после 12 смысла увеличивать глубину нет.

#### 2.6. Bagging.
Here starts the ensembling part.

First we will use the __Bagging__ approach. Build an ensemble of $N$ algorithms varying N from $N_{min}=2$ to $N_{max}=100$ (with step 5).

We will build two ensembles: of logistic regressions and of decision trees.

*Comment: each ensemble should be constructed from models of the same family, so logistic regressions should not be mixed up with decision trees.*


*Hint 1: To build a __Bagging__ ensebmle varying the ensemble size efficiently you might generate $N_{max}$ subsets of `train` data (of the same size as the original dataset) using bootstrap procedure once. Then you train a new instance of logistic regression/decision tree with optimal hyperparameters you estimated before on each subset (so you train it from scratch). Finally, to get an ensemble of $N$ models you average the $N$ out of $N_{max}$ models predictions.*

*Hint 2: sklearn might help you with this taks. Some appropriate function/class might be out there.*

* Plot `f1` and `accuracy` scores plots w.r.t. the size of the ensemble.

* Briefly analyse the plot. What is the optimal number of algorithms? Explain your answer.

* How do you think, are the hyperparameters for the decision trees you found in 2.5 optimal for trees used in ensemble?
"""

# YOUR CODE HERE



"""#### 2.7. Random Forest
Now we will work with the Random Forest (its `sklearn` implementation).

* * Plot `f1` and `accuracy` scores plots w.r.t. the number of trees in Random Forest.

* What is the optimal number of trees you've got? Is it different from the optimal number of logistic regressions/decision trees in 2.6? Explain the results briefly.
"""

from sklearn.ensemble import RandomForestClassifier

score_f1 = []
score_acc = []
for i in range(10, 200, 5):
  RF = RandomForestClassifier(n_estimators = i)
  res = cross_validate(RF, data_PCA, target, cv = 5, scoring = ['f1_macro', 'accuracy'])
  score_f1.append(res['test_f1_macro'].mean())
  score_acc.append(res['test_accuracy'].mean())

x = [j for j in range(10, 200, 5)]
plt.plot(x, score_f1)

x = [j for j in range(10, 200, 5)]
plt.plot(x, score_acc)

"""Оптимальный размер леса при 70 деревьях.

#### 2.8. Learning curve
Your goal is to estimate, how does the model behaviour change with the increase of the `train` dataset size.

* Split the training data into 10 equal (almost) parts. Then train the models from above (Logistic regression, Desicion Tree, Random Forest) with optimal hyperparameters you have selected on 1 part, 2 parts (combined, so the train size in increased by 2 times), 3 parts and so on.

* Build a plot of `accuracy` and `f1` scores on `test` part, varying the `train` dataset size (so the axes will be score - dataset size.

* Analyse the final plot. Can you make any conlusions using it?
"""

# YOUR CODE HERE